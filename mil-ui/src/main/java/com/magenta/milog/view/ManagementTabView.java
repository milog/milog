package com.magenta.milog.view;

import com.magenta.milog.MgtServiceRemote;
import com.magenta.milog.model.VProperty;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by danil on 13.11.17.
 */

@Named("mgtView")
@ViewScoped
public class ManagementTabView implements Serializable {

    @EJB(lookup = "java:global/milog/ManagementService!com.magenta.milog.MgtServiceRemote")
    private MgtServiceRemote mgtService;

    private VProperty defaultProp = new VProperty();

    public Long getLogsInCache() {
        return mgtService.getCountLogsInCache();
    }

    public Long getCountDurableLogs() {
        return mgtService.getCountDurableLogs();
    }

    public List<VProperty> getSettings() {
        List<VProperty> properties = new ArrayList<>();
        mgtService.getSettings().forEach((s, s2) -> properties.add(new VProperty(s, s2)));
        return properties;
    }

    public void updateSettings(VProperty property) {
        if (property.key == null || property.key.isEmpty()) {
            return;
        }
        mgtService.updateSettings(property.key, property.value);
        defaultProp = new VProperty();
    }

    public void removeSettings(VProperty property) {
        if (property.key == null || property.key.isEmpty()) {
            return;
        }
        mgtService.removeSettings(property.key);
    }

    public VProperty getDefaultProp() {
        return defaultProp;
    }
}
