package com.magenta.milog.view;

import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.model.EventLazyDataModel;
import org.primefaces.model.LazyDataModel;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by danil on 08.11.17.
 */

@Named("eventsView")
@ViewScoped
public class DurableEventsView implements Serializable {
    private Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Inject
    private EventLazyDataModel dataModel;
    private Integer activeTab = 0;
    private List<SelectItem> actions;
    private String selectedAction;

    @PostConstruct
    public void init() {
        actions = new ArrayList<>();
        Map<String, Set<String>> availableActions = dataModel.getAvailableActions();
        availableActions.forEach((group, as) -> {
            SelectItemGroup g = new SelectItemGroup(group);
            g.setSelectItems(as.stream().map(s -> new SelectItem(s, s)).toArray(SelectItem[]::new));
            actions.add(g);
        });
    }

    public LazyDataModel<LogEvent> getEvents() {
        return dataModel;
    }

    public String convertTime(long time) {
        Date date = new Date(time);
        return format.format(date);
    }

    public Integer getActiveTab() {
        return activeTab;
    }

    public void setActiveTab(Integer activeTab) {
        this.activeTab = activeTab;
    }

    public List<SelectItem> getActions() {
        return actions;
    }

    public String getSelectedAction() {
        return selectedAction;
    }

    public void setSelectedAction(String selectedAction) {
        this.selectedAction = selectedAction;
    }
}
