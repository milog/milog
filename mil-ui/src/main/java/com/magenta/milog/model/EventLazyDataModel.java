package com.magenta.milog.model;

import com.magenta.milog.EventServiceRemote;
import com.magenta.milog.dto.FilterCondition;
import com.magenta.milog.dto.ListResult;
import com.magenta.milog.dto.LogEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

import javax.ejb.EJB;
import java.io.Serializable;
import java.util.*;

/**
 * Created by danil on 08.11.17.
 */
public class EventLazyDataModel extends LazyDataModel<LogEvent> implements Serializable {

    private static final Map<String, String> fieldMap = new HashMap<>();

    {
        fieldMap.put("startTime", "client_start_time");
        fieldMap.put("endTime", "client_end_time");
    }

    @EJB(lookup = "java:global/milog/EventService!com.magenta.milog.EventServiceRemote")
    private EventServiceRemote eventService;

    @Override
    public List<LogEvent> load(int first, int pageSize, String sortField,
                               SortOrder sortOrder, Map<String, Object> filters) {

        if (sortField != null) {
            String s = fieldMap.get(sortField);
            if (s != null) {
                sortField = s;
            }
        }

        ListResult listResult = eventService.loadEvents(first, pageSize == 0 ? 100 : pageSize, sortField, getSort(sortOrder), getFilterCondition(filters));
        List<LogEvent> list = listResult.getRows();
        this.setRowCount(listResult.getTotal());

        return list;
    }

    @Override
    public List<LogEvent> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        return super.load(first, pageSize, multiSortMeta, filters);
    }

    private static com.magenta.milog.dto.SortOrder getSort(SortOrder sortOrder) {
        return sortOrder.equals(SortOrder.DESCENDING) ? com.magenta.milog.dto.SortOrder.DESC : com.magenta.milog.dto.SortOrder.ASC;
    }

    private static List<FilterCondition> getFilterCondition(Map<String, Object> filters) {
        if (filters.isEmpty()) {
            return null;
        }

        List<FilterCondition> conditions = new ArrayList<>();
        filters.forEach((s, o) -> {
            String condition = "=";
            if (s.equals("startTime")) {
                s = "client_start_time";
                condition = ">=";
            } else if (s.equals("endTime")) {
                s = "client_end_time";
                condition = "<=";
            }
            conditions.add(new FilterCondition(s, o, condition));
        });
        return conditions;
    }

    public Map<String, Set<String>> getAvailableActions() {
        return eventService.getAvailableActions();
    }
}
