package com.magenta.milog.model;

/**
 * Created by danil on 14.11.17.
 */
public class VProperty {
    public String key;
    public String value;

    public VProperty() {
    }

    public VProperty(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
