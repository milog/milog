package com.magenta.milog;

import javax.ejb.Remote;
import java.util.Map;

/**
 * Created by danil on 13.11.17.
 */

@Remote
public interface MgtServiceRemote {

    Long getCountLogsInCache();

    Long getCountDurableLogs();

    Map<String, String> getSettings();

    void updateSettings(String key, String value);

    void removeSettings(String key);
}
