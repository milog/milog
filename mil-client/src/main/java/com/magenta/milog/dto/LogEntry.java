package com.magenta.milog.dto;

import java.io.Serializable;
import java.util.UUID;

public interface LogEntry extends Serializable {
    UUID getUid();
}
