package com.magenta.milog.dto;

import java.io.Serializable;

/**
 * Created by danil on 09.11.17.
 */
public class FilterCondition implements Serializable {
    private String field;
    private Object value;
    private String condition;

    public FilterCondition(String field, Object value, String condition) {
        this.field = field;
        this.value = value;
        this.condition = condition;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
