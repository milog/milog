package com.magenta.milog.dto;

import java.io.Serializable;

/**
 * Created by danil on 09.11.17.
 */
public enum SortOrder implements Serializable{
    ASC,
    DESC,
}
