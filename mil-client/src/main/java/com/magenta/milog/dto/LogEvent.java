package com.magenta.milog.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.xml.bind.annotation.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

/**
 * Created by danil on 05.10.17.
 */

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
public class LogEvent implements LogEntry {

    @JsonIgnore
    @XmlElement(name = "id", nillable = true)
    @JsonProperty("id")
    @XmlTransient
    private Long id;

    @XmlElement(name = "pid")
    @JsonProperty("pid")
    private UUID processId;

    @XmlElement(name = "seq")
    @JsonProperty("seq")
    private Integer sequence = 0;

    @XmlElement(name = "sti")
    @JsonProperty("sti")
    private Long startTime;

    @XmlElement(name = "eti")
    @JsonProperty("eti")
    private Long endTime;

    @XmlElement(name = "tz")
    @JsonProperty("tz")
    private String timeZone;

    @XmlElement(name = "sid")
    @JsonProperty("sid")
    private String sessionId;

    @XmlElement(name = "gid")
    @JsonProperty("gid")
    private String group;

    @XmlElement(name = "tid")
    @JsonProperty("tid")
    private String txId;

    @XmlElement(name = "hst")
    @JsonProperty("hst")
    private String host;

    @XmlElement(name = "act")
    @JsonProperty("act")
    private String actor;

    @XmlElement(name = "aid")
    @JsonProperty("aid")
    private String appId;

    @XmlElement(name = "avn")
    @JsonProperty("avn")
    private String appVin;

    @XmlElement(name = "an")
    @JsonProperty("an")
    private String action;

    @XmlElement(name = "ac")
    @JsonProperty("ac")
    private String account;

    @XmlElement(name = "msg")
    @JsonProperty("msg")
    private String message;

    @XmlElement(name = "tp")
    @JsonProperty("tp")
    private Integer type = LogType.INFO.ordinal();

    @XmlElement(name = "ps")
    @JsonProperty("ps")
    private Set<Param> params;

    public LogEvent() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Set<Param> getParams() {
        return params;
    }

    public void setParams(Set<Param> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

    public static TypeReference getTypeReference() {
        return new TypeReference<List<LogEvent>>() {
        };
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void addParam(String name, Object value) {
        if (this.params == null) {
            this.params = new HashSet<>();
        }
        this.params.add(new Param(name, value));
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public void incSequence() {
        sequence++;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setType(LogType type) {
        this.type = type.ordinal();
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getAppVin() {
        return appVin;
    }

    public void setAppVin(String appVin) {
        this.appVin = appVin;
    }

    public UUID getProcessId() {
        return processId;
    }

    public void setProcessId(UUID processId) {
        this.processId = processId;
    }

    @Override
    public UUID getUid() {
        return this.processId;
    }
}
