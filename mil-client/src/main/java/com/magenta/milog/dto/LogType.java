package com.magenta.milog.dto;

/**
 * Created by danil on 07.11.17.
 */
public enum LogType {
    INFO, ERROR
}
