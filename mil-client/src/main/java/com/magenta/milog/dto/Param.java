package com.magenta.milog.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

/**
 * Created by danil on 09.10.17.
 */

@XmlRootElement(name = "ps")
@XmlAccessorType(XmlAccessType.FIELD)
public class Param {

    @XmlElement(name = "nm")
    @JsonProperty("nm")
    private String name;

    @XmlElement(name = "tp")
    @JsonProperty("tp")
    private String type;

    @XmlElement(name = "vl")
    @JsonProperty("vl")
    private Object value;

    public Param() {
    }

    public Param(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public static TypeReference getTypeReference() {
        return new TypeReference<Set<Param>>() {
        };
    }

    @Override
    public int hashCode() {
        return this.getName() == null ? this.hashCode() : this.getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Param) {
            String name = ((Param) obj).getName();
            if (this.getName() == null || name == null) {
                return false;
            }
            return this.getName().equalsIgnoreCase(((Param) obj).getName());
        }
        return super.equals(obj);
    }
}
