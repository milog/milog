package com.magenta.milog.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by danil on 08.11.17.
 */
public class ListResult implements Serializable {

    private List<LogEvent> rows;
    private int total = 0;

    public List<LogEvent> getRows() {
        return rows;
    }

    public void setRows(List<LogEvent> rows) {
        this.rows = rows;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ListResult(List<LogEvent> rows, int total) {
        this.rows = rows;
        this.total = total;
    }
}
