package com.magenta.milog;

import com.magenta.milog.dto.FilterCondition;
import com.magenta.milog.dto.ListResult;
import com.magenta.milog.dto.SortOrder;

import javax.ejb.Remote;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by danil on 08.11.17.
 */

@Remote
public interface EventServiceRemote {

    ListResult loadEvents(int first, int pageSize, String sortField, SortOrder sortOrder, List<FilterCondition> filters);

    Map<String, Set<String>> getAvailableActions();
}
