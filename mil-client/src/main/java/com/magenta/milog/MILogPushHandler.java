package com.magenta.milog;

import com.magenta.milog.dto.LogEntry;

import java.util.List;

/**
 * Created by danil on 31.10.17.
 */
public interface MILogPushHandler {
    void callbackOnPush(List<LogEntry> events, int responseCode);

    void errorOnPush(String msg);
}
