package com.magenta.milog;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magenta.milog.dto.LogEntry;

import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by danil on 30.10.17.
 */

public class MILogPushService {

    private static URL SERVICE;
    private static Lock LOCK = new ReentrantLock();
    private static final ObjectMapper OMAPPER = new ObjectMapper();
    private static final ConcurrentHashMap<UUID, StoredLog> EVENTS = new ConcurrentHashMap<>(100);
    private static MILogPushHandler PUSH_HANDLER = new MILogPushHandler() {
        @Override
        public void callbackOnPush(List<LogEntry> events, int responseCode) {

        }

        @Override
        public void errorOnPush(String msg) {

        }
    };

    static class StoredLog {
        long updated;
        LogEntry log;

        StoredLog(LogEntry log) {
            this.updated = System.currentTimeMillis();
            this.log = log;
        }
    }

    static {
        String logServer = System.getenv("MIL_HOST");
        if (logServer != null) {
            try {
                SERVICE = new URL(logServer + "/milog/api/v1/events/put/");
                ManagedScheduledExecutorService managedScheduledExecutorService = InitialContext.doLookup("java:comp/DefaultManagedScheduledExecutorService");
                managedScheduledExecutorService.scheduleAtFixedRate(getTask(), 5, 5, TimeUnit.SECONDS);
            } catch (NamingException e) {
                Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(getTask(), 5, 5, TimeUnit.SECONDS);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    private static Runnable getTask() {
        return new Runnable() {
            @Override
            public void run() {
                push();
            }
        };
    }

    private static void push() {
        try {
            if (EVENTS.isEmpty()) {
                return;
            }
            List<LogEntry> events = getEvents();
            if (events.isEmpty()) {
                return;
            }
            HttpURLConnection con = (HttpURLConnection) SERVICE.openConnection();
            try {
                con.setRequestMethod("PUT");
                con.setRequestProperty("Accept-Charset", "UTF-8");
                con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");

                con.setConnectTimeout(3000);
                con.setReadTimeout(3000);
                con.setDoOutput(true);
                con.setInstanceFollowRedirects(false);
                OMAPPER.writeValue(con.getOutputStream(), events);
                int responseCode = con.getResponseCode();
                PUSH_HANDLER.callbackOnPush(events, responseCode);
            } catch (Exception e) {
                PUSH_HANDLER.errorOnPush(e.getMessage());
            } finally {
                con.disconnect();
            }
        } catch (IOException e) {
            PUSH_HANDLER.errorOnPush(e.getMessage());
        }
    }

    public static void add(LogEntry log, Callable callbackOnExists) throws Exception {
        try {
            LOCK.lock();
            if (EVENTS.containsKey(log.getUid())) {
                callbackOnExists.call();
            }
            EVENTS.put(log.getUid(), new StoredLog(log));
        } finally {
            LOCK.unlock();
        }
    }

    public static void add(LogEntry log) {
        EVENTS.put(log.getUid(), new StoredLog(log));
    }

    private static List<LogEntry> getEvents() {
        try {
            LOCK.lock();
            List<LogEntry> events = new ArrayList<>();
            long time = System.currentTimeMillis() - 5000;
            Iterator<StoredLog> iterator = EVENTS.values().iterator();
            while (iterator.hasNext()) {
                StoredLog storedLog = iterator.next();
                if (storedLog.updated < time) {
                    events.add(storedLog.log);
                    iterator.remove();
                }
            }
            return events;
        } finally {
            LOCK.unlock();
        }
    }

    public static void setPushHandler(MILogPushHandler pushHandler) {
        PUSH_HANDLER = pushHandler;
    }
}
