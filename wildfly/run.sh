docker rm wildfly
docker run --name=wildfly --ip 172.30.0.4 -p 8080:8080 -p 8787:8787 \
-v /home/danil/projects/logger/wildfly/container/data:/opt/wildfly/standalone/data/ \
-v /home/danil/projects/logger/wildfly/container/log:/opt/wildfly/standalone/log/ \
-v /home/danil/projects/logger/wildfly/container/deployments:/opt/wildfly/standalone/deployments/ \
-e PG_HOST="172.30.0.3" \
--net=mx-bi wildfly:10.1.0.Final