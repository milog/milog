                                    RUN COMPOSE
docker-compose up

                                      BUILD IMAGE
* **step 1:** ./docker/ext/docker-compose up \
 _load and create image from external hub_  
* **step 2:** ./docker/loc/docker-compose up \
 _load, create image from local hub and run container_  
                
**manual build:** docker build -t wildfly:10.1.0.Final .