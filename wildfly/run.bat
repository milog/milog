docker rm wildfly
docker run --name=wildfly --ip 172.30.0.4 -p 8080:8080 -p 8787:8787 ^
-v C:/projects/milog/wildfly/container/data:/opt/wildfly/standalone/data/ ^
-v C:/projects/milog/wildfly/container/log:/opt/wildfly/standalone/log/ ^
-v C:/projects/milog/wildfly/container/deployments:/opt/wildfly/standalone/deployments/ ^
-e PG_HOST="172.30.0.3" ^
--net=mx-mil wildfly_loc:10.1.0.Final