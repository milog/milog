package com.magenta.milog.service;

import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.DBName;
import com.magenta.milog.helper.MILHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;
import com.magenta.milog.qualifier.Property;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@ApplicationScoped
public class SettingsService {

    private Properties properties;

    private static final String SELECT_QUERY = "SELECT key, value FROM settings";
    private static final String DELETE_QUERY = "DELETE FROM settings WHERE key = ?";
    private static final String UPDATE_QUERY = "INSERT INTO settings (key, value) VALUES (?, ?) ON CONFLICT (key) DO UPDATE SET VALUE = ?";

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private MLogDecorator LOG;

    @Inject
    private WarehouseDBMgtService warehouseService;

    @Property
    @Produces
    public String produceString(final InjectionPoint ip) {
        return this.properties.getProperty(getKey(ip));
    }

    @Property
    @Produces
    public int produceInt(final InjectionPoint ip) {
        return Integer.valueOf(this.properties.getProperty(getKey(ip)));
    }

    @Property
    @Produces
    public boolean produceBoolean(final InjectionPoint ip) {
        return Boolean.valueOf(this.properties.getProperty(getKey(ip)));
    }

    @PostConstruct
    public void init() {
        this.properties = new Properties();
        try {
            ResultSet resultSet = connection.createStatement().executeQuery(SELECT_QUERY);
            while (resultSet.next()) {
                String key = resultSet.getString(1);
                String value = resultSet.getString(2);
                this.properties.setProperty(key, value);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public Map<String, String> getSettings() {
        return new HashMap<String, String>((Map) properties);
    }

    public void updateProperty(String key, String value) {
        try {
            if (MILHelper.isEmpty(key)) {
                LOG.error("settings key must not be empty");
                return;
            }
            PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY);
            ps.setString(1, key);
            ps.setString(2, value);
            ps.setString(3, value);
            ps.execute();
            this.properties.setProperty(key, value);
            LOG.infof("settings '%s' has new value '%s'", key, value);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getKey(final InjectionPoint ip) {
        Property annotation = ip.getAnnotated().getAnnotation(Property.class);
        return (annotation != null && !annotation.value().isEmpty()) ? annotation.value() : ip.getMember().getName();
    }

    public void launch(@Observes @DB(DBName.WAREHOUSE) @Launch String event) {
        try (Connection con = warehouseService.getConnection()) {
            MILHelper.applyChangeLogs(con, "settings_table");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeSettings(String key) {
        try {
            if (MILHelper.isEmpty(key)) {
                LOG.error("settings key must not be empty");
                return;
            }
            PreparedStatement ps = connection.prepareStatement(DELETE_QUERY);
            ps.setString(1, key);
            ps.execute();
            this.properties.remove(key);
            LOG.infof("settings '%s' has been removed", key);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
