package com.magenta.milog.service.stash;

import com.magenta.milog.dto.LogEntry;

import java.util.List;

public interface IStash {

    void addList(List<? extends LogEntry> list);

    List<LogEntry> getList(int count);

    Integer getSize();
}
