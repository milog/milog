package com.magenta.milog.service.stash;

import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.LogEntry;
import com.magenta.milog.dto.StringLog;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

@ApplicationScoped
public class FileStash implements IStash {

    private DB db;
    private HTreeMap<UUID, String> cache;
    private AtomicBoolean isLoaded = new AtomicBoolean(false);
    private static final String LOG_DB = System.getProperty("jboss.server.base.dir") + "/data/store/";

    @Inject
    private MLogDecorator LOG;

    @PostConstruct
    public void postConstruct() {
        try {
            Files.createDirectories(Paths.get(LOG_DB));
            db = DBMaker.fileDB(LOG_DB + "log.db")
                    .closeOnJvmShutdownWeakReference()
                    .fileMmapEnableIfSupported()
                    .fileChannelEnable()
                    .checksumHeaderBypass()
                    .fileMmapPreclearDisable()
                    .cleanerHackEnable()
                    .make();
            cache = db.hashMap("log", Serializer.UUID, Serializer.STRING)
                    .createOrOpen();
            db.getStore().fileLoad();
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public void addList(List<? extends LogEntry> logEntries) {
        for (LogEntry logEntry : logEntries) {
            cache.put(UUID.randomUUID(), logEntry.toString());
        }
        db.commit();
    }

    @Override
    public List<LogEntry> getList(int count) {
        if (this.isLoaded.get()) {
            this.cache.clear();
            return Collections.emptyList();
        }
        this.isLoaded.set(true);
        if (this.cache.isEmpty()) {
            return Collections.emptyList();
        }
        final List<LogEntry> list = new ArrayList<>(this.cache.size());
        this.cache.forEach((uuid, s) ->
                list.add(new StringLog(s))
        );
        this.cache.clear();
        db.commit();
        LOG.infof("loaded %s entries", list.size());
        return list;
    }

    @Override
    public Integer getSize() {
        return cache.size();
    }

    @PreDestroy
    public void preDestroy() {
        db.close();
    }
}
