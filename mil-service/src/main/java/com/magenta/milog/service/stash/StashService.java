package com.magenta.milog.service.stash;

import com.magenta.milog.dto.LogEntry;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class StashService {

    @Inject
    @Any
    private Instance<IStash> stashes;

    public void put(List<? extends LogEntry> list) {
        for (IStash stash : stashes) {
            stash.addList(list);
        }
    }

    public List<LogEntry> get(int count) {
        List<LogEntry> result = new ArrayList<>(count);
        for (IStash stash : stashes) {
            if (result.size() < count) {
                result.addAll(stash.getList(count - result.size()));
            }
        }
        return result;
    }

    public Long getSize() {
        Long count = 0L;
        for (IStash stash : stashes) {
            count += stash.getSize();
        }
        return count;
    }
}
