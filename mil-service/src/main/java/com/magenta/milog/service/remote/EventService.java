package com.magenta.milog.service.remote;

import com.magenta.milog.EventServiceRemote;
import com.magenta.milog.dto.*;
import com.magenta.milog.helper.MILHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.service.processing.ActionProcessingService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Created by danil on 08.11.17.
 */

@Stateless
public class EventService implements EventServiceRemote {

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private ActionProcessingService actionService;

    @Override
    public ListResult loadEvents(int first, int pageSize, String sortField, SortOrder sortOrder, List<FilterCondition> filters) {
        List<LogEvent> events = new ArrayList<>();
        Integer total = 0;
        try {
            Statement statement = connection.createStatement();

            String whereQuery = buildWhere(filters);
            ResultSet resultSet = statement.executeQuery("SELECT count(id) FROM durable_events " + whereQuery);
            while (resultSet.next()) {
                total = resultSet.getInt(1);
            }

            if (total != 0) {
                StringBuilder query = new StringBuilder("SELECT action, account, session_id, host, client_start_time, client_end_time, actor, app_id, app_vin FROM durable_events ");
                query.append(whereQuery);
                if (MILHelper.isEmpty(sortField)) {
                    sortField = "client_start_time";
                    sortOrder = SortOrder.DESC;
                }
                query.append(" ORDER BY ").append(sortField).append(" ").append(sortOrder.name());
                query.append(" LIMIT ").append(pageSize).append(" OFFSET ").append(first);

                resultSet = statement.executeQuery(query.toString());
                while (resultSet.next()) {
                    events.add(getLogEvent(resultSet));
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ListResult(events, total);
    }

    @Override
    public Map<String, Set<String>> getAvailableActions() {
        return actionService.getActions();
    }

    private static LogEvent getLogEvent(ResultSet rawLogs) throws SQLException {
        LogEvent event = new LogEvent();
        event.setAction(rawLogs.getString(1));
        event.setAccount(rawLogs.getString(2));
        event.setSessionId(rawLogs.getString(3));
        event.setHost(rawLogs.getString(4));
        event.setStartTime(rawLogs.getTimestamp(5).getTime());
        event.setEndTime(rawLogs.getTimestamp(6).getTime());
        event.setActor(rawLogs.getString(7));
        event.setAppId(rawLogs.getString(8));
        event.setAppVin(rawLogs.getString(9));
        return event;
    }

    private String buildWhere(List<FilterCondition> filters) {
        if (filters == null || filters.isEmpty()) {
            return "";
        }

        String and = "";
        StringBuilder sb = new StringBuilder("WHERE ");
        for (FilterCondition filter : filters) {
            sb.append(and);

            Object value = filter.getValue();
            if (value instanceof String) {
                value = String.format("'%s'", value);
            }
            if (value instanceof Date) {
                value = String.format("'%s'", new Timestamp(((Date) value).getTime()));
            }
            sb.append(filter.getField()).append(filter.getCondition()).append(value);
            and = " AND ";
        }
        return sb.toString();
    }
}
