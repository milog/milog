package com.magenta.milog.service.processing;

import com.magenta.milog.dto.LogEntry;

import java.util.List;

public interface IProcessingService {

    void preProcessing(List<LogEntry> entries);

    void postProcessing(List<? extends LogEntry> objects);

}
