package com.magenta.milog.service;


import com.magenta.milog.dto.DBName;
import com.magenta.milog.dto.TField;
import com.magenta.milog.helper.TableNamesHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by danil on 06.10.17.
 */

@ApplicationScoped
public class WarehouseDBMgtService {

    @Resource(lookup = "java:jboss/datasources/WarehouseDS")
    private DataSource dataSource;

    @Inject
    @DB(DBName.WAREHOUSE)
    @Launch
    private Event<String> dbLaunch;

    private Map<String, Set<String>> dbInfo = Collections.synchronizedMap(new HashMap<>());

    public void init(@Observes @Initialized(ApplicationScoped.class) Object init) {
        try (Connection con = getConnection()){
            dbLaunch.fire("");
            DatabaseMetaData md = con.getMetaData();
            ResultSet tables = md.getTables(null, "public", "%", null);
            while (tables.next()) {
                String table = tables.getString(3);
                ResultSet columns = md.getColumns(null, "public", table, null);
                Set<String> cachedCols = new HashSet<>();
                dbInfo.put(table, cachedCols);
                while (columns.next()) {
                    String column = columns.getString(4);
                    cachedCols.add(column);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Produces
    @RequestScoped
    @DB(DBName.WAREHOUSE)
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void close(@Disposes @DB(DBName.WAREHOUSE) Connection connection) throws SQLException {
        connection.close();
    }

    public synchronized void addTable(String table, Set<TField> fields) {
        table = TableNamesHelper.getTableName(table);
        Set<String> existsColls = Optional.ofNullable(dbInfo.get(table)).orElse(new HashSet<>());
        Set<String> newCalls = fields.stream().map(TField::getName).collect(Collectors.toSet());
        StringBuilder sql = null;
        if (existsColls.isEmpty()) {
            sql = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append(table).append("(");
            String sep = "";
            StringBuilder uniques = new StringBuilder();
            for (TField field : fields) {
                sql.append(sep).append(field.getName()).append(" ").append(field.getType());
                sep = ", ";
                if (field.getUnique()) {
                    uniques.append(",UNIQUE (").append(field.getName()).append(")");
                }
            }
            sql.append(uniques).append(")");
        } else {
            newCalls.removeAll(existsColls);
            if (!newCalls.isEmpty()) {
                sql = new StringBuilder("ALTER TABLE ").append(table);
                String sep = "";
                for (TField field : fields) {
                    if (newCalls.contains(field.getName())) {
                        sql.append(sep).append(" ADD COLUMN ").append(field.getName()).append(" ").append(field.getType());
                        sep = ", ";
                    }
                }
            }
        }
        if (sql != null) {
            try (Connection connection = getConnection()) {
                PreparedStatement ps = connection.prepareStatement(sql.toString());
                ps.executeUpdate();
                ps.close();
                connection.close();
                existsColls.addAll(newCalls);
                dbInfo.put(table, existsColls);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
