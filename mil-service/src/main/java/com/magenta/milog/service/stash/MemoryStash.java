package com.magenta.milog.service.stash;


import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.LogEntry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by danil on 05.10.17.
 */

@ApplicationScoped
public class MemoryStash implements IStash {
    private List<LogEntry> objects = Collections.synchronizedList(new ArrayList<>());

    @Inject
    private MLogDecorator LOG;

    @Override
    @SuppressWarnings("unchecked")
    public void addList(List<? extends LogEntry> list) {
        this.objects.addAll(list);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<LogEntry> getList(int count) {
        List<LogEntry> entries = new ArrayList<>(objects.subList(0, Math.min(count, objects.size())));
        objects.removeAll(entries);
        LOG.infof("loaded %s entries", entries.size());
        return entries;
    }

    @Override
    public Integer getSize() {
        return objects.size();
    }
}
