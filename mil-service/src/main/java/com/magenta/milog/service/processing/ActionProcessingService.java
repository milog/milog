package com.magenta.milog.service.processing;

import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.DBName;
import com.magenta.milog.dto.LogEntry;
import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.helper.MILHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;
import com.magenta.milog.service.WarehouseDBMgtService;
import com.magenta.milog.service.WarehouseDBObserverService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by danil on 10.11.17.
 */

@ApplicationScoped
public class ActionProcessingService extends AbstractProcessingService implements WarehouseDBObserverService {
    private static final String SELECT_QUERY = "SELECT group_id, action FROM actions";
    private static final String INSERT_QUERY = "INSERT INTO actions (group_id, action) VALUES (?, ?)";
    private static final Map<String, Set<String>> actionsMap = new HashMap<>();
    private static final Lock lock = new ReentrantLock();

    @Inject
    private WarehouseDBMgtService warehouseService;

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private EventProcessingService eventProcessingService;

    @Inject
    private MLogDecorator LOG;

    public Map<String, Set<String>> getActions() {
        return actionsMap;
    }

    @PostConstruct
    public void init() {
        try {
            ResultSet rs = connection.createStatement().executeQuery(SELECT_QUERY);
            while (rs.next()) {
                String group = rs.getString(1);
                Set<String> acs = actionsMap.computeIfAbsent(group, k -> new HashSet<>());
                acs.add(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void launch(@Observes @DB(DBName.WAREHOUSE) @Launch String event) {
        try (Connection con = warehouseService.getConnection()) {
            MILHelper.applyChangeLogs(con, "actions_table");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<LogEvent> getSuitable(List<LogEntry> objects) {
        return eventProcessingService.getSuitable(objects);
    }

    @Override
    public void postProcessing(List<? extends LogEntry> entries) {
        if (entries.isEmpty()) {
            return;
        }
        LOG.info("start processing");
        AtomicLong added = new AtomicLong(0);
        Map<String, Set<String>> groupedActions = new HashMap<>();
        List<LogEvent> logEvents = (List<LogEvent>) entries;
        for (LogEvent event : logEvents) {
            Set<String> actions = groupedActions.computeIfAbsent(event.getGroup(), s -> new HashSet<>());
            actions.add(event.getAction());
        }

        groupedActions.forEach((group, newActions) -> {
            Set<String> exists = actionsMap.get(group);
            if (exists != null) {
                newActions.removeAll(exists);
            }
            if (!newActions.isEmpty()) {
                try {
                    PreparedStatement insertQuery = connection.prepareStatement(INSERT_QUERY);
                    for (String action : newActions) {
                        insertQuery.setString(1, group);
                        insertQuery.setString(2, action);
                        insertQuery.addBatch();
                    }
                    insertQuery.executeBatch();

                    Set<String> acs = actionsMap.computeIfAbsent(group, k -> new HashSet<>());
                    acs.addAll(newActions);

                    added.addAndGet(newActions.size());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        LOG.infof("added %s actions", added.get());
    }
}
