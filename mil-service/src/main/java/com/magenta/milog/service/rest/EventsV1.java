package com.magenta.milog.service.rest;


import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.service.stash.StashService;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by danil on 04.10.17.
 */

@Path("/api/v1/events")
public class EventsV1 {

    @Inject
    private StashService stashService;

//    @PUT
//    @Path("/put")
//    @Produces(MediaType.APPLICATION_JSON)
//    public void add(@Context HttpServletRequest request) {
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            List<LogEntry> events = mapper.readValue(request.getInputStream(), new TypeReference<List<SingleEvent>>() {
//            });
//            LOG.info(String.format("received %s events", events.size()));
//            stashService.put(events);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    @PUT
    @Path("/put")
    @Produces(MediaType.APPLICATION_JSON)
    public void add(List<LogEvent> events, @Context HttpServletRequest request) {
        for (LogEvent event : events) {
            event.setHost(request.getRemoteHost());
        }
        stashService.put(events);
    }
}
