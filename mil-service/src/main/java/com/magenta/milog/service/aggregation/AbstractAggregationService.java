package com.magenta.milog.service.aggregation;

import com.magenta.milog.service.CursorService;

import javax.inject.Inject;

public abstract class AbstractAggregationService {

    @Inject
    private CursorService cursorService;

    protected void setCursor(String src, Long cursor) {
        cursorService.setCursor(getServiceName(), src, cursor);
    }

    protected Long getCursor(String src) {
        return cursorService.getCursor(getServiceName(), src);
    }

    protected String getServiceName() {
        return getClass().getSimpleName();
    }
}
