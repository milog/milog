package com.magenta.milog.service;

import com.magenta.milog.dto.DBName;
import com.magenta.milog.helper.MILHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CursorService {

    private static final String UPDATE_QUERY = "INSERT INTO cursor (service, src, idx) VALUES (?, ?, ?) ON CONFLICT (src) DO UPDATE SET idx = ?";
    private static final String SELECT_QUERY = "SELECT idx FROM cursor WHERE service = ? AND src = ?";

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private WarehouseDBMgtService warehouseService;

    public void setCursor(String service, String source, Long cursor) {
        try {
            PreparedStatement ps = connection.prepareStatement(UPDATE_QUERY);
            ps.setString(1, service);
            ps.setString(2, source);
            ps.setLong(3, cursor);
            ps.setLong(4, cursor);
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void launch(@Observes @DB(DBName.WAREHOUSE) @Launch String event) {
        try (Connection con = warehouseService.getConnection()) {
            MILHelper.applyChangeLogs(con, "cursor_table");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Long getCursor(String serviceName, String source) {
        Long result = 0L;
        try {
            PreparedStatement ps = connection.prepareStatement(SELECT_QUERY);
            ps.setString(1, serviceName);
            ps.setString(2, source);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()) {
                result = resultSet.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }
}
