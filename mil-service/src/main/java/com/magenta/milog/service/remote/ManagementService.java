package com.magenta.milog.service.remote;

import com.magenta.milog.MgtServiceRemote;
import com.magenta.milog.dto.DBName;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.service.SettingsService;
import com.magenta.milog.service.stash.StashService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

/**
 * Created by danil on 13.11.17.
 */

@Stateless
public class ManagementService implements MgtServiceRemote {

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private StashService stashService;

    @Inject
    private SettingsService settingsService;

    @Override
    public Long getCountLogsInCache() {
        return stashService.getSize();
    }

    @Override
    public Long getCountDurableLogs() {
        long total = 0;
        try {
            ResultSet resultSet = connection.createStatement().executeQuery("SELECT id FROM durable_events ORDER BY id DESC LIMIT 1");
            while (resultSet.next()) {
                total = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;
    }

    @Override
    public Map<String, String> getSettings() {
        return settingsService.getSettings();
    }

    @Override
    public void updateSettings(String key, String value) {
        settingsService.updateProperty(key, value);
    }

    @Override
    public void removeSettings(String key) {
        settingsService.removeSettings(key);
    }
}
