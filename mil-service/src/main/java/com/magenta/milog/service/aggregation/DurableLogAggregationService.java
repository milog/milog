package com.magenta.milog.service.aggregation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.DBName;
import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.dto.Param;
import com.magenta.milog.dto.TField;
import com.magenta.milog.helper.MILHelper;
import com.magenta.milog.helper.TableNamesHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;
import com.magenta.milog.service.WarehouseDBMgtService;
import com.magenta.milog.service.WarehouseDBObserverService;

import javax.ejb.*;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.IOException;
import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static javax.ejb.LockType.READ;

@Singleton
@Local(WarehouseDBObserverService.class)
public class DurableLogAggregationService extends AbstractAggregationService implements WarehouseDBObserverService {

    private static final String SELECT_QUERY = "SELECT id, pid, sequence, group_id, action, session_id, host, start_time, end_time, time_zone, actor, app_id, app_vin, account, type, params FROM raw_events WHERE id > ?";
    private static final String INSERT_QUERY = "INSERT INTO durable_events (pid, action, session_id, host, utc_start_time, utc_end_time, client_start_time, client_end_time, duration, actor, app_id, app_vin, account, type) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON CONFLICT (pid) DO UPDATE SET utc_end_time = ?, client_end_time = ?, duration = ?";
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Inject
    private WarehouseDBMgtService warehouseService;

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private MLogDecorator LOG;

    @Lock(READ)
    @Schedule(hour = "*", minute = "*", second = "*/30")
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    protected void aggregate() {
        LOG.info("aggregation start");
        AtomicLong cursor = new AtomicLong(getCursor("raw-events"));
        try {
            PreparedStatement statement = connection.prepareStatement(SELECT_QUERY);
            statement.setLong(1, cursor.get());
            ResultSet rawLogs = statement.executeQuery();
            List<LogEvent> rawEvents = new ArrayList<>();

            while (rawLogs.next()) {
                Optional<LogEvent> op = getLogEvent(rawLogs);
                op.ifPresent(event -> {
                    cursor.set(Long.max(event.getId(), cursor.get()));
                    rawEvents.add(event);
                });

            }

            if (rawEvents.isEmpty()) {
                LOG.info("no events for aggregation");
                return;
            }

            Collection<LogEvent> completedEvents = MILHelper.getCompletedEvents(rawEvents);
            insertEvents(completedEvents);
            insertEventsParams(completedEvents);
            setCursor("raw-events", cursor.get());

            LOG.infof("aggregated %s events", completedEvents.size());
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }

        LOG.info("aggregation end");
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void launch(@Observes @DB(DBName.WAREHOUSE) @Launch String event) {
        try (Connection con = warehouseService.getConnection()) {
            MILHelper.applyChangeLogs(con, "durable_events_table");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private static Optional<LogEvent> getLogEvent(ResultSet rawLogs) throws SQLException {
        try {
            LogEvent event = new LogEvent();
            event.setId(rawLogs.getLong(1));
            event.setProcessId((UUID) rawLogs.getObject(2));
            event.setSequence(rawLogs.getInt(3));
            event.setGroup(rawLogs.getString(4));
            event.setAction(rawLogs.getString(5));
            event.setSessionId(rawLogs.getString(6));
            event.setHost(rawLogs.getString(7));
            event.setStartTime(rawLogs.getTimestamp(8).getTime());
            event.setEndTime(rawLogs.getTimestamp(9).getTime());
            event.setTimeZone(rawLogs.getString(10));
            event.setActor(rawLogs.getString(11));
            event.setAppId(rawLogs.getString(12));
            event.setAppVin(rawLogs.getString(13));
            event.setAccount(rawLogs.getString(14));
            event.setType(rawLogs.getInt(15));

            String params = rawLogs.getString(16);
            if (!MILHelper.isEmpty(params)) {
                event.setParams(objectMapper.readValue(params, Param.getTypeReference()));
            }
            return Optional.of(event);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private static void addEventToQuery(PreparedStatement insertQuery, LogEvent event) throws SQLException {
        insertQuery.setObject(1, event.getProcessId());
        insertQuery.setString(2, event.getAction());
        insertQuery.setString(3, event.getSessionId());
        insertQuery.setString(4, event.getHost());
        insertQuery.setTimestamp(5, new Timestamp(event.getStartTime()));
        insertQuery.setTimestamp(6, new Timestamp(event.getEndTime()));

        ZoneId zone = ZoneId.of(event.getTimeZone());
        LocalDateTime clientSt = LocalDateTime.ofInstant(Instant.ofEpochMilli(event.getStartTime()), zone);
        LocalDateTime clientEt = LocalDateTime.ofInstant(Instant.ofEpochMilli(event.getEndTime()), zone);
        insertQuery.setTimestamp(7, Timestamp.valueOf(clientSt));
        insertQuery.setTimestamp(8, Timestamp.valueOf(clientEt));

        insertQuery.setLong(9, event.getEndTime() - event.getStartTime());
        insertQuery.setString(10, event.getActor());
        insertQuery.setString(11, event.getAppId());
        insertQuery.setString(12, event.getAppVin());
        insertQuery.setString(13, event.getAccount());
        insertQuery.setInt(14, event.getType());

        /* FOR UPDATE */
        insertQuery.setTimestamp(15, new Timestamp(event.getEndTime()));
        insertQuery.setTimestamp(16, Timestamp.valueOf(clientEt));
        insertQuery.setLong(17, event.getEndTime() - event.getStartTime());

        insertQuery.addBatch();
    }

    private void insertEvents(Collection<LogEvent> events) throws SQLException {
        if (events.isEmpty()) {
            return;
        }
        PreparedStatement ps = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
        for (LogEvent event : events) {
            addEventToQuery(ps, event);
        }
        ps.executeBatch();
        ResultSet ids = ps.getGeneratedKeys();
        for (LogEvent completedEvent : events) {
            ids.next();
            completedEvent.setId(ids.getLong(1));

        }
        ps.close();
    }

    private void insertEventsParams(Collection<LogEvent> events) throws SQLException {
        Statement paramsSt = null;
        Set<TField> fields = new LinkedHashSet<>();
        for (LogEvent event : events) {
            if (event.getParams() != null && !event.getParams().isEmpty()) {
                if (paramsSt == null) {
                    paramsSt = connection.createStatement();
                }

                StringBuilder values = new StringBuilder();
                StringBuilder update = new StringBuilder();

                fields.clear();
                for (Param param : event.getParams()) {
                    fields.add(new TField(param.getName()));
                    values.append("\'").append(param.getValue().toString()).append("\'").append(",");
                    update.append(TableNamesHelper.getFiledName(param.getName())).append("=\'").append(param.getValue().toString()).append("\'").append(",");
                }
                fields.add(new TField("event_id", "bigint", true));
                values.append(event.getId());

                if (update.length() > 0) {
                    update.deleteCharAt(update.length() - 1);
                }

                warehouseService.addTable(event.getGroup(), fields);
                String query = String.format("INSERT INTO %s (%s) VALUES(%s) ON CONFLICT (event_id) DO UPDATE SET %s;",
                        TableNamesHelper.getTableName(event.getGroup()),
                        fields.stream().map(TField::getName).collect(Collectors.joining(",")),
                        values,
                        update
                );
                paramsSt.addBatch(query);
            }
        }

        if (paramsSt != null) {
            paramsSt.executeBatch();
            paramsSt.close();
        }
    }
}
