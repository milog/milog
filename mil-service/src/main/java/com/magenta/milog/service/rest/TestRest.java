package com.magenta.milog.service.rest;

import com.magenta.milog.MILogPushHandler;
import com.magenta.milog.MILogPushService;
import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.LogEntry;
import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.dto.Param;
import com.magenta.milog.helper.TestsHelper;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Path("/api/test")
public class TestRest {

    private static final MLogDecorator LOG = MLogDecorator.create(TestRest.class);

    static {
        MILogPushService.setPushHandler(new MILogPushHandler() {
            @Override
            public void callbackOnPush(List<LogEntry> events, int responseCode) {
                LOG.infof("Response Code: %s; Pushed %s events", responseCode, events.size());
            }

            @Override
            public void errorOnPush(String msg) {
                LOG.error(msg);
            }
        });
    }

    @POST
    @Path("/sql")
    @Produces({"application/json"})
    public List<LogEvent> sql(@FormParam("sql") String sql) {
        List<LogEvent> events = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            LogEvent event = new LogEvent();
//            event.setParams(new ArrayList<Param>() {
//                {add(new Param("a1", 3L));}
//            });
//            events.add(event);
//        }
        return events;
    }


    @GET
    @Path("/FirstLastEventTest")
    @Produces({"application/json"})
    public void firstLastEventTest() {
        LogEvent start = TestsHelper.copyEvent(new LogEvent() {{
            setParams(new HashSet<Param>() {
                {
                    add(new Param("param-1-name", "param-1-value"));
                    add(new Param("param-2-name", "param-2-value"));
                }
            });
        }});
        MILogPushService.add(start);

        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LogEvent end = new LogEvent() {{
            setProcessId(start.getProcessId());
            setEndTime(System.currentTimeMillis());
            setParams(new HashSet<Param>() {
                {
                    add(new Param("param-3-name", "param-3-value"));
                    add(new Param("param-4-name", "param-4-value"));
                }
            });
        }};
        MILogPushService.add(TestsHelper.copyEvent(end));
    }

}
