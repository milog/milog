//package com.magenta.milog.service.pull;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.magenta.milog.dto.LogEntry;
//import com.magenta.milog.dto.SingleEvent;
//import com.magenta.milog.helper.MILHelper;
//import com.magenta.milog.service.stash.StashService;
//import org.jboss.logging.Logger;
//
//import javax.ejb.Lock;
//import javax.ejb.Schedule;
//import javax.ejb.Singleton;
//import javax.inject.Inject;
//import java.io.IOException;
//import java.util.Collections;
//import java.util.List;
//
//import static javax.ejb.LockType.READ;
//
//@Singleton
//public class MaxoptraPullService {
//
//    private static Logger LOG = Logger.getLogger(MaxoptraPullService.class);
//    private ObjectMapper objectMapper = new ObjectMapper();
//
//    @Inject
//    private StashService stashService;
//
//    @Lock(READ)
//    @Schedule(hour = "*", minute = "*", second = "*/30", persistent = false)
//    protected void pull() {
//        try {
//            LOG.info("pull start");
//            MILHelper.sendPost("http://localhost:8080/milog/api/test/sql", Collections.singletonMap("sql", getSql()), (status, payload) -> {
//                if (payload.isEmpty()) {
//                    return;
//                }
//                try {
//                    List<LogEntry> events = objectMapper.readValue(payload, SingleEvent.getTypeReference());
//                    if (!events.isEmpty()) {
//                        LOG.info(String.format("pulled %s entries", events.size()));
////                        stashService.put(events);
//                    }
//                } catch (IOException e) {
//                    LOG.error(e.getMessage());
//                }
//            });
//        } catch (IOException e) {
//            LOG.error(e.getMessage());
//        }
//        LOG.info("pull end");
//    }
//
//    private String getSql() {
//        return "select * from events";
//    }
//}
