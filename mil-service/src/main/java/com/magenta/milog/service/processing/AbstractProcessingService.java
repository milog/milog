package com.magenta.milog.service.processing;

import com.magenta.milog.dto.LogEntry;

import java.util.List;

public abstract class AbstractProcessingService implements IProcessingService {

    @Override
    public void preProcessing(List<LogEntry> entries) {
        List<? extends LogEntry> objects = getSuitable(entries);
        if (objects.isEmpty()) {
            return;
        }
        postProcessing(objects);
    }

    protected abstract List<? extends LogEntry> getSuitable(List<LogEntry> objects);
}
