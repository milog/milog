package com.magenta.milog.service;


import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.LogEntry;
import com.magenta.milog.service.processing.ProcessingService;
import com.magenta.milog.service.stash.StashService;

import javax.ejb.Lock;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.util.List;

import static javax.ejb.LockType.READ;


/**
 * Created by danil on 05.10.17.
 */

@Singleton
public class RetranslatorTimerService {

    @Inject
    private MLogDecorator LOG;

    @Inject
    private StashService stashService;

    @Inject
    private ProcessingService processingService;

    @Lock(READ)
    @Schedule(hour = "*", minute = "*", second = "*/20", persistent = false)
    protected void shift() {
        LOG.info("shift start");
        List<LogEntry> objects = stashService.get(1000);
        if (!objects.isEmpty()) {
            processingService.processing(objects);
        }
        LOG.info("shift end");
    }
}
