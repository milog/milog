package com.magenta.milog.service;

import com.magenta.milog.dto.DBName;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;

import javax.enterprise.event.Observes;

public interface WarehouseDBObserverService {

    void launch(@Observes @DB(DBName.WAREHOUSE) @Launch String event);
}
