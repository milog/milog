package com.magenta.milog.service.processing;

import com.magenta.milog.dto.LogEntry;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.List;

public class ProcessingService {

    @Inject
    @Any
    private Instance<IProcessingService> processingServices;

    public void processing(List<LogEntry> entries) {
        for (IProcessingService processingService : processingServices) {
            if (!entries.isEmpty()) {
                processingService.preProcessing(entries);
            }
        }
    }
}
