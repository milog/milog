package com.magenta.milog.service.processing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.magenta.milog.decorator.MLogDecorator;
import com.magenta.milog.dto.DBName;
import com.magenta.milog.dto.LogEntry;
import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.dto.StringLog;
import com.magenta.milog.helper.MILHelper;
import com.magenta.milog.qualifier.DB;
import com.magenta.milog.qualifier.Launch;
import com.magenta.milog.service.WarehouseDBMgtService;
import com.magenta.milog.service.WarehouseDBObserverService;

import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by danil on 05.10.17.
 */
public class EventProcessingService extends AbstractProcessingService implements WarehouseDBObserverService {
    private ObjectMapper mapper = new ObjectMapper();
    private static final String INSERT_QUERY = "INSERT INTO raw_events (pid, sequence, group_id, action, tx_id, session_id, host, start_time, end_time, time_zone, actor, app_id, app_vin, account, message, type, params) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, CAST(? AS JSONB))";

    @Inject
    @DB(DBName.WAREHOUSE)
    private Connection connection;

    @Inject
    private WarehouseDBMgtService warehouseService;

    @Inject
    private MLogDecorator LOG;

    @Override
    public void postProcessing(List<? extends LogEntry> entries) {
        if (entries.isEmpty()) {
            return;
        }
        LOG.info("start processing");
        try {
            PreparedStatement ps = connection.prepareStatement(INSERT_QUERY);

            Collection<LogEvent> events = MILHelper.getCompletedEvents((List<LogEvent>) entries);
            for (LogEvent event : events) {
                try {
                    ps.setObject(1, event.getProcessId());
                    ps.setInt(2, event.getSequence());
                    ps.setString(3, event.getGroup());
                    ps.setString(4, event.getAction());
                    ps.setString(5, event.getTxId());
                    ps.setString(6, event.getSessionId());
                    ps.setString(7, event.getHost());
                    ps.setTimestamp(8, new Timestamp(event.getStartTime()));
                    ps.setTimestamp(9, new Timestamp(event.getEndTime()));
                    ps.setString(10, event.getTimeZone());
                    ps.setString(11, event.getActor());
                    ps.setString(12, event.getAppId());
                    ps.setString(13, event.getAppVin());
                    ps.setString(14, event.getAccount());
                    ps.setString(15, event.getMessage());
                    ps.setInt(16, event.getType());
                    ps.setObject(17, event.getParams() != null ? mapper.writeValueAsString(event.getParams()) : null);
                    ps.addBatch();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            ps.executeBatch();
            ps.close();
            LOG.infof("processed %s events", events.size());
        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    @Override
    public List<LogEvent> getSuitable(List<LogEntry> entries) {
        List<LogEvent> events = new ArrayList<>();

        Iterator<LogEntry> iterator = entries.iterator();
        while (iterator.hasNext()) {
            LogEntry entry = iterator.next();
            if (entry instanceof LogEvent) {
                events.add((LogEvent) entry);
            } else if (entry instanceof StringLog) {
                try {
                    events.add(mapper.readValue(((StringLog) entry).getValue(), LogEvent.class));
                    iterator.remove();
                    entries.add(entry);
                } catch (IOException ignore) {
                }
            }
        }
        return events;
    }

    public void launch(@Observes @DB(DBName.WAREHOUSE) @Launch String event) {
        try (Connection con = warehouseService.getConnection()) {
            MILHelper.applyChangeLogs(con, "raw_events_table");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
