package com.magenta.milog.helper;

import com.magenta.milog.dto.LogEvent;
import com.magenta.milog.dto.LogType;

import java.util.HashSet;
import java.util.UUID;

/**
 * Created by danil on 07.11.17.
 */
public class TestsHelper {

    public static LogEvent copyEvent(LogEvent newEvent) {
        LogEvent event = new LogEvent();
        event.setProcessId(newEvent.getProcessId() == null ? UUID.randomUUID() : newEvent.getProcessId());
        event.setSequence(1);
        event.setSessionId("session_" + UUID.randomUUID());
        event.setGroup("group1");
        event.setHost("127.0.0.1");
        event.setStartTime(System.currentTimeMillis());
        event.setEndTime(System.currentTimeMillis() + 5000);
        event.setActor("Test");
        event.setAppId("TestApp");
        event.setAppVin("12.12.12");
        event.setTimeZone("UTC");
        event.setAccount("test_account");
        event.setMessage("test msg");
        event.setType(LogType.INFO);
        event.setAction("batch");
        event.setParams(new HashSet<>());
        event.getParams().addAll(newEvent.getParams());
        return event;
    }
}
