package com.magenta.milog.helper;

/**
 * Created by danil on 09.10.17.
 */
public class TableNamesHelper {

    public static String getTableName(String field) {
        return field.trim().toLowerCase()
                .replaceAll("-", "_")
                .replaceAll("\\s+", "_")
                + "_params";
    }

    public static String getFiledName(String field) {
        return field.trim().toLowerCase()
                .replaceAll("-", "_")
                .replaceAll("\\s+", "_");
    }
}
