package com.magenta.milog.helper;

import com.magenta.milog.dto.LogEvent;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

import java.sql.Connection;
import java.util.*;

public class MILHelper {
    public static boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    public static Collection<LogEvent> getCompletedEvents(List<LogEvent> events) {
        Map<UUID, LogEvent> eventMap = new HashMap<>(events.size());
        for (LogEvent nextEvent : events) {
            LogEvent prevEvent = eventMap.get(nextEvent.getProcessId());
            if (prevEvent == null) {
                eventMap.put(nextEvent.getProcessId(), nextEvent);
                continue;
            }
            if (nextEvent.getSequence() > prevEvent.getSequence()) {
                prevEvent.setSequence(nextEvent.getSequence());
                prevEvent.setEndTime(nextEvent.getEndTime());
            }
            if (prevEvent.getParams() == null) {
                prevEvent.setParams(new HashSet<>());
            }
            if (nextEvent.getParams() != null) {
                prevEvent.getParams().addAll(nextEvent.getParams());
            }
        }
        return eventMap.values();
    }

    public static void applyChangeLogs(Connection connection, String... changeLogs) {
        try {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));
            for (String changelog : changeLogs) {
                Liquibase liquibase = new liquibase.Liquibase(String.format("/liquibase/changelog/%s.xml", changelog), new ClassLoaderResourceAccessor(MILHelper.class.getClassLoader()), database);
                liquibase.update(new Contexts(), new LabelExpression());
            }
        } catch (LiquibaseException e) {
            e.printStackTrace();
        }
    }

}
