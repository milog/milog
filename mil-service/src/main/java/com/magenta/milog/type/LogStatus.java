package com.magenta.milog.type;

public enum LogStatus {
    IN_QUEUE,
    IN_PROCESS,
    PROCESSED
}
