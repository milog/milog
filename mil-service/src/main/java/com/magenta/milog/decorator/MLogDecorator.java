package com.magenta.milog.decorator;

import org.jboss.logging.Logger;


/**
 * Created by danil on 07.11.17.
 */
public class MLogDecorator {
    private Logger logger;

    private MLogDecorator(String c) {
        this.logger = org.jboss.logging.Logger.getLogger(c);
    }

    public static MLogDecorator create(Class c) {
        return new MLogDecorator(c.getName());
    }

    public static MLogDecorator create(String c) {
        return new MLogDecorator(c);
    }

    public void info(String msg) {
        logger.infof(";- %s -", msg);
    }

    public void infof(String format, Object... params) {
        logger.infof(String.format(";- %s -", format), params);
    }

    public void error(String msg) {
        logger.error(msg);
    }
}
