package com.magenta.milog.dto;

import java.util.UUID;

public class StringLog implements LogEntry {
    private String value;

    public StringLog(String log) {
        this.value = log;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public UUID getUid() {
        return null;
    }
}
