package com.magenta.milog.dto;

import com.magenta.milog.helper.TableNamesHelper;

public class TField {
    private String name;
    private String type = "varchar(100)";
    private Boolean unique = false;

    public TField(String name) {
        setName(name);
    }

    public TField(String name, String type) {
        setName(name);
        this.type = type;
    }

    public TField(String name, String type, Boolean unique) {
        this.name = name;
        this.type = type;
        this.unique = unique;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = TableNamesHelper.getFiledName(name);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getUnique() {
        return unique;
    }

    public void setUnique(Boolean unique) {
        this.unique = unique;
    }
}
