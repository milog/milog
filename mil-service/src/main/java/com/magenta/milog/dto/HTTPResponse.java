package com.magenta.milog.dto;

@FunctionalInterface
public interface HTTPResponse {
    void response(int status, String payload);
}
