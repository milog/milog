package com.magenta.milog;

import com.magenta.milog.decorator.MLogDecorator;
import org.jboss.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class MILogApplication extends Application {

    @Produces
    public MLogDecorator produceLogger(InjectionPoint injectionPoint) {
        return MLogDecorator.create(injectionPoint.getMember().getDeclaringClass().getName());
    }
}
